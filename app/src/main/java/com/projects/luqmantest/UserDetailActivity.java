package com.projects.luqmantest;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.os.Bundle;
import android.widget.TextView;

import com.projects.luqmantest.databinding.ActivityUserDetailBinding;

public class UserDetailActivity extends AppCompatActivity {

    private String KEYWORD_USERNAME = "USERNAME";
    private String KEYWORD_PASSWORD = "PASSWORD";
    private String KEYWORD_EMAIL = "EMAIL";

    private String KEYWORD_USERMODEL="USER_MODEL";

    private UserModel userModel;
    private String userName, password, email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_detail);
        ActivityUserDetailBinding activityUserDetailBinding= DataBindingUtil.setContentView(this,R.layout.activity_user_detail);
        userModel=getIntent().getParcelableExtra(KEYWORD_USERMODEL);

        activityUserDetailBinding.setUserModel(userModel);
    }
}