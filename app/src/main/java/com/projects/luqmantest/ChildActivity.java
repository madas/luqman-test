package com.projects.luqmantest;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Firdaus Al Fidai on 25/04/2022.
 */

public class ChildActivity extends AppCompatActivity {

    private List<UserModel> userModelList = new ArrayList<>();
    private UserAdapter userAdapter;

    private RecyclerView rvUser;

    private TextView tvDesc;
    private Button btnSend;

    private String descContent = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_child);

        if (getIntent().hasExtra("KEY_CONTENT")) {
            descContent = getIntent().getStringExtra("KEY_CONTENT");
        }

        tvDesc = findViewById(R.id.tv_content);
        tvDesc.setText(descContent);

        btnSend = findViewById(R.id.bt_send);
        btnSend.setOnClickListener(onClick -> {
            Intent callbackIntent = new Intent();
            callbackIntent.putExtra("KEY_CALLBACK", descContent);
            setResult(RESULT_OK, callbackIntent);
            finish();
        });

//        UserModel userA = new UserModel();
//        UserModel userB = new UserModel();
//        UserModel userC = new UserModel();
//        userModelList.add(userA);
//        userModelList.add(userB);
//        userModelList.add(userC);

        userAdapter = new UserAdapter(userModelList);

        rvUser = findViewById(R.id.rv_user);
        rvUser.setLayoutManager(new LinearLayoutManager(this));
        rvUser.setAdapter(userAdapter);
    }
}
