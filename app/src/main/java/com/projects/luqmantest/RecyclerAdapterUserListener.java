package com.projects.luqmantest;

/**
 * Created by Firdaus Al Fidai on 22/01/2023.
 */
public interface RecyclerAdapterUserListener {
    void onUserSelected(UserModel userModel);
}
