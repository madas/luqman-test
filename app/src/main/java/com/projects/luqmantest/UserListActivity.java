package com.projects.luqmantest;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.projects.luqmantest.databinding.ActivityUserListBinding;
import com.projects.luqmantest.databinding.ItemUserListBinding;

import java.util.ArrayList;
import java.util.List;

public class UserListActivity extends AppCompatActivity implements RecyclerAdapterUserListener{

    private ActivityUserListBinding binding;
    private BaseRecyclerAdapter<UserModel> baseRvUserAdapter;

    private List<UserModel> userModelList = new ArrayList<>();

    private String KEYWORD_USERMODEL="USER_MODEL";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_user_list);

        UserModel userModelA = new UserModel("Username A", "Email A", "Password A",true);
        UserModel userModelB = new UserModel("Username B", "Email B", "Password B",false);
        UserModel userModelC = new UserModel("Username C", "Email C", "Password C",true);
        UserModel userModelD = new UserModel("Username D", "Email D", "Password D",false);
        UserModel userModelE = new UserModel("Username E", "Email E", "Password E",true);

        userModelList.add(userModelA);
        userModelList.add(userModelB);
        userModelList.add(userModelC);
        userModelList.add(userModelD);
        userModelList.add(userModelE);


        baseRvUserAdapter = new BaseRecyclerAdapter<>(
                userModelList,
                R.layout.item_user_list,
                ((viewHolder, userModel) -> {
                    ItemUserListBinding binding = (ItemUserListBinding) viewHolder.getBinding();
                    binding.setUserModel(userModel);
                    binding.cvMain.setOnClickListener(onClick -> onUserSelected(userModel));
                })
        );
        binding.setUserAdapter(baseRvUserAdapter);
    }

    @Override
    public void onUserSelected(UserModel userModel) {
        Log.v("username",userModel.getUsername());
        Intent intent=new Intent(UserListActivity.this,UserDetailActivity.class);

//        intent.putExtra(KEYWORD_USERNAME,userModel.getUsername());
//        intent.putExtra(KEYWORD_PASSWORD,userModel.getPassword());
//        intent.putExtra(KEYWORD_EMAIL,userModel.getEmail());
        intent.putExtra(KEYWORD_USERMODEL, userModel);

        startActivity(intent);


    }
}