package com.projects.luqmantest;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.projects.luqmantest.databinding.ItemUserListBinding;

import java.util.List;


public class RecyclerAdapterUser extends RecyclerView.Adapter<RecyclerAdapterUser.ViewHolder> {

    private List<UserModel> localDataSet;
    private RecyclerAdapterUserListener listener;

    /**
     * Provide a reference to the type of views that you are using
     * (custom ViewHolder)
     */
    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ItemUserListBinding binding;

        public ViewHolder(ItemUserListBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bind(UserModel userModel) {
            binding.setUserModel(userModel);
            binding.executePendingBindings();
        }

    }

    /**
     * Initialize the dataset of the Adapter
     *
     * @param dataSet String[] containing the data to populate views to be used
     *                by RecyclerView
     */
    public RecyclerAdapterUser(List<UserModel> dataSet) {
        localDataSet = dataSet;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        ItemUserListBinding binding = DataBindingUtil.inflate(
                LayoutInflater.from(viewGroup.getContext()),
                R.layout.item_user_list,
                viewGroup,
                false
        );

        return new ViewHolder(binding);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        final UserModel userModel = localDataSet.get(position);
        viewHolder.bind(userModel);
        viewHolder.binding.cvMain.setOnClickListener(view -> listener.onUserSelected(userModel));
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return localDataSet.size();
    }

    public void setListener(RecyclerAdapterUserListener listener) {
        this.listener = listener;
    }
}

