package com.projects.luqmantest;

import org.json.JSONException;
import org.json.JSONObject;

public class DigimonModel {
    private String name;
    private String level;
    private String imageUrl;

    public DigimonModel(JSONObject responseObj) {
        try {
            if (responseObj.has("name")) setName(responseObj.getString("name"));
            if (responseObj.has("level")) setLevel(responseObj.getString("level"));
            if (responseObj.has("img")) setImageUrl(responseObj.getString("img"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
