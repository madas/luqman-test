package com.projects.luqmantest;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.os.Bundle;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.projects.luqmantest.databinding.ActivityApiListBinding;
import com.projects.luqmantest.databinding.ItemDigimonListBinding;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ApiListActivity extends AppCompatActivity {
    private ActivityApiListBinding binding;
    private final List<DigimonModel> digimonModelList = new ArrayList<>();
    private BaseRecyclerAdapter<DigimonModel> digimonAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_api_list);
        digimonAdapter = new BaseRecyclerAdapter<>(
                digimonModelList,
                R.layout.item_digimon_list,
                ((viewHolder, digimonModel) -> {
                    ItemDigimonListBinding binding = (ItemDigimonListBinding) viewHolder.getBinding();
                    binding.setDigimonModel(digimonModel);
                })
        );

        binding.setDigimonAdapter(digimonAdapter);
        callApi();
    }

    private void callApi() {
        RequestQueue queue = Volley.newRequestQueue(this);
        String url = "https://digimon-api.vercel.app/api/digimon";
        JsonArrayRequest request = new JsonArrayRequest(
                Request.Method.GET,
                url,
                null,
                response -> {
                    try {
                        for (int i = 0; i < response.length(); i++) {
                            JSONObject responseObj = response.getJSONObject(i);
                            DigimonModel digimonModel = new DigimonModel(responseObj);
                            digimonModelList.add(digimonModel);
                            Log.v("response", responseObj.toString());
                        }

                        digimonAdapter.setMainData(digimonModelList);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                },
                error -> Log.v("responseError", error.toString())
        );

        queue.add(request);
    }
}