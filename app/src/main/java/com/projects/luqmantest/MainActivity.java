package com.projects.luqmantest;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private int callbackCode = 9827;

    private EditText etDesc;
    private Button btnSend;
    private TextView tvContent;

    private UserModel userModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        etDesc = findViewById(R.id.et_desc);
        btnSend = findViewById(R.id.bt_send);
        tvContent = findViewById(R.id.tv_content);

        btnSend.setOnClickListener(onClick -> {
            String descContent = etDesc.getText().toString();

            Intent newActivityIntent = new Intent(MainActivity.this, ChildActivity.class);
            newActivityIntent.putExtra("KEY_CONTENT", descContent);
            startActivityForResult(newActivityIntent, callbackCode);
        });

//        userModel = new UserModel("madas", "madas@gmail.com", "123456");

//        Log.v("userModelValue", String.format("%s %s %s", userModel.getUsername(), userModel.getEmail(), userModel.getHandphone()));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            if (requestCode == callbackCode) {
                if (data == null) return;

                if (data.hasExtra("KEY_CALLBACK")){
                    String callbackContent = data.getStringExtra("KEY_CALLBACK");
                    tvContent.setText(callbackContent);
                }
            }
        }
    }
}